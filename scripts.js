document.addEventListener('DOMContentLoaded', function() {
    fetch('https://gitlab.com/api/v4/users/danibopacic/projects')
        .then(response => response.json())
        .then(projects => {
            const projectsContainer = document.querySelector('.gitlab-projects');
            projects.forEach(project => {
                const projectCard = document.createElement('div');
                projectCard.className = 'project-card bg-white rounded-lg shadow-lg p-6 transition-transform transform hover:-translate-y-2 dark:bg-gray-700';
                projectCard.innerHTML = `
                    <h3 class="text-xl font-semibold text-indigo-600 dark:text-teal-400">${project.name}</h3>
                    <p class="mt-2">${project.description || 'No description available.'}</p>
                    <a href="${project.web_url}" target="_blank" class="block mt-4 bg-indigo-600 text-white text-center py-2 rounded-md hover:bg-teal-500 transition-colors">View Project</a>
                `;
                projectsContainer.appendChild(projectCard);
            });
        });

    // Dark Mode Toggle
    const darkModeToggle = document.getElementById('darkModeToggle');
    const toggleDarkMode = () => {
        document.documentElement.classList.toggle('dark');
        localStorage.setItem('theme', document.documentElement.classList.contains('dark') ? 'dark' : 'light');
        darkModeToggle.innerHTML = document.documentElement.classList.contains('dark') ? '<i class="fas fa-sun"></i>' : '<i class="fas fa-moon"></i>';
    };

    darkModeToggle.addEventListener('click', toggleDarkMode);

    // Set the initial theme based on local storage
    if (localStorage.getItem('theme') === 'dark') {
        document.documentElement.classList.add('dark');
        darkModeToggle.innerHTML = '<i class="fas fa-sun"></i>';
    } else {
        darkModeToggle.innerHTML = '<i class="fas fa-moon"></i>';
    }
});
